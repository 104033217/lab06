function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function () {
        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert" and clean input field
        let [email, password] = [txtEmail.value, txtPassword.value];
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then((m) => {
            create_alert("success", "◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣");
            console.log(m);
            // window.history.pushState(null, "login", "/lab06/index.html");
            window.location.href = window.location.origin + "/lab06/index.html";
            // test@gmail.com password
            // test2@gmail.com password
        })
        .catch(function(err){
            create_alert("error", [err.code, err.message].join(" ◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣ "));
        });
    });

    btnGoogle.addEventListener('click', function () {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"
        let provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider)
        .then((result) => {
            create_alert("success", "success");
            let token = result.credential.accessToken;
            let user = result.user;
        })
        .catch((err) => {
            create_alert("error", [err.code, err.message].join(" ◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣ "));
        });
    });

    btnSignUp.addEventListener('click', function () {        
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        let [email, password] = [txtEmail.value, txtPassword.value];
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .catch((err) => {
            create_alert("error", [err.code, err.message].join(" ◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣崩潰 "));
        })
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};